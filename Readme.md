# Dotfiles

These are my dot files. Feel free to copy/use/pr/help with them : )

I use zsh and [Prezto](https://github.com/sorin-ionescu/prezto) with the [pure](https://github.com/sindresorhus/pure) theme.

I use vscode as my IDE.

# Install

```
./install.sh
```