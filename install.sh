#!/bin/bash

# Install brew
if [[ ! $(which brew) ]]; then
    echo "Installing brew"
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Install zsh
if [[ ! $(which zsh) ]]; then
    echo "Installing zsh"
    brew install zsh
fi

# # Install languages
# for l in python3 go; do
#     if [[ ! $(which ${l}) ]]; then
#         echo "Installing ${l}"
#         brew install $l
#     fi
# done

# # Install python tools
# for p in virtualenv; do 
#     if [[ ! $(which ${p}) ]]; then
#         echo "Installing ${p}"
#         pip install $p
#     fi
# done

# Install git tools
if [[ ! $(which git) ]]; then
    echo "Installing git"
    brew install git
    cd ~/
    wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh
    chmod +x git-completion.zsh
fi

# Install nerd fonts
brew tap caskroom/fonts
brew cask install font-hack-nerd-font

# Fuzzzzzy find
brew install fzf

# Install terminal
brew cask install alacritty

# Install links and settings files in the correct location / home dir

# Link .zshrc and .zpreztorc
echo "Linking .zshrc and .zpreztorc"
ln -sf $HOME/code/dotfiles/.zshrc $HOME/.zshrc
ln -sf $HOME/code/dotfiles/.zpreztorc $HOME/.zpreztorc

# Link pure shell prompt config
echo "Linking pure.zsh config"
ln -sf $HOME/code/dotfiles/pure.zsh $HOME/.zprezto/modules/prompt/external/pure/pure.zsh

# Link alacritty config
ln -sf $HOME/code/dotfiles/.config $HOME/.config

# Link vscode
echo "Linking vscode user settings"
ln -sf $HOME/code/dotfiles/vscode/ $HOME/Library/Application\ Support/Code/User