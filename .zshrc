# .zshrc
# Gabe Ochoa's zshrc file
# Maintainer Gabe Ochoa <gabeochoa@gmail.com>

# prints debugging info
ENABLE_DEBUGGING=false

# User local Path
PATH=$PATH:/usr/local/bin

# Where the dotfiles are located to load
export DOTFILES=$HOME/code/dotfiles

if [[ ENABLE_DEBUGGING == "true" ]]; then
  # run the debugging, if enabled
  echo "Debugging enabled"
  source $DOTFILES/zshrc.lib/debugging.zsh
fi

# Prezto ENVs
export POWERLEVEL9K_MODE='nerdfont-complete'
export PURE_CMD_MAX_EXEC_TIME="1"

KUBECTL_PURE_PROMPT_ENABLE="true"
if [[ $(hostname) == "Gabes-MBP.lan" ]]; then
  export KUBECTL_PURE_PROMPT_ENABLE="false"
fi

# Reload prompt every second so that the time display works
TMOUT=5
TRAPALRM() {
    zle reset-prompt
}

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# DO NOT LET ZSH AUTO CORRECT THINGS
unsetopt correct_all

# Completions
# source ~/code/dotfiles/completions/git-completion.zsh
# source <(kubectl completion zsh)
autoload -U compinit && compinit
# zmodload -i ~/code/dotfiles/completions/git-completion.zsh

# Python virtualenvs
export WORKON_HOME=~/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python
# source /usr/local/bin/virtualenvwrapper.sh

# Rust path and bin
export PATH="$PATH:$HOME/.cargo/bin"
if [[ -z $HOME/.cargo/env ]]; then
  source $HOME/.cargo/env
fi

# GO path and bin
export GOPATH=$HOME/code
export PATH=$PATH:$HOME/code
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/usr/local/opt/go/libexec/bin
export PATH=$PATH:/usr/local/go/bin
export GO111MODULE=on

# Tiny-care term :)

function tct() {
  docker run -it -v ~/code:/code \
              -v ~/.gitconfig:/root/.gitconfig \
              -e TTC_SAY_BOX=parrot \
              -e TTC_REPOS=/code \
              -e TTC_REPOS_DEPTH=10 \
              -e TTC_WEATHER=10014 \
              -e TTC_CELSIUS=false \
              -e TTC_APIKEYS=false \
              -e TTC_GITBOT='gitlog' \
              dasrecht/tiny-care-terminal-docker
}

# Default AWS profile
AWS_PROFILE=sqsp

# Top level aliases
alias reload='source ~/.zshrc'
alias editalias='sublime ~/.slimzsh/aliases.zsh.local'
alias editenv='sublime ~/.zshenv'
alias zshconfig='sublime ~/.zshrc'
alias ll='ls -l'

# Override some git alias
alias undo='git reset HEAD~'

# Kube alias
alias kdp='k config use-context eqx-dal-prod > /dev/null 2>&1 && k --context eqx-dal-prod'
alias kds='k config use-context eqx-dal-stage > /dev/null 2>&1 && k'
alias kdc='k config use-context eqx-dal-corp > /dev/null 2>&1 && k'
alias kns='k config use-context drt-ewr-stage > /dev/null 2>&1 && k'
alias knp='k config use-context drt-ewr-prod > /dev/null 2>&1 && k'
alias knc='k config use-context drt-ewr-corp > /dev/null 2>&1 && k'
alias kdsandbox='k config use-context eqx-dal-sandbox > /dev/null 2>&1 && k'
alias kmini='k config use-context minikube > /dev/null 2>&1 && k'

# Dev location alias
alias code-go-github='cd $HOME/code/src/github.com'
alias code-go-ss='cd $HOME/code/src/code.squarespace.net/sre'

# file: git config --global -e

# git go (go get) [See: https://regex101.com/r/0akEWc/2]
# git clone <repo> into a path determined by the repo' domain, owner, and name
# given repos like:
#   https://bitbucket.org/some_owner/some_project.git
#   git@bitbucket.org:some_owner/some_project.git
#   https://hkatz@code.squarespace.net/scm/some_owner/some_project.git
#   git@github.com:some_owner/some_project.git
#   https://github.com/some_owner/some_project.git
#
# git go will properly call git clone <repo> /Some/path/to/code/<site>/<owner>/<project>/
#
# For example:
#   git go ssh://git@code.squarespace.net:7999/some_owner/some_project.git
# Becomes:
#   git clone ssh://git@code.squarespace.net:7999/some_owner/some_project.git /Some/path/to/Code/code.squarespace.net/some_owner/some_project/
# alias go = "!f(){ git clone \"$1\" $(echo \"$1\"|perl -pe 's{^(https?|git|ssh)(://|@)(?:[^@/:]+@)?([^/:]+)[/:](?:scm|\\d+)?/?([^/:]+)/(.+?)(.git)?$}{/Users/gochoa/code/\\3/\\4/\\5}');};f"

# kube cluster viewer
alias kube-cluster-viewer="docker run  -p 8080:8080 -e CLUSTERS=http://docker.for.mac.localhost:8001 hjacobs/kube-ops-view && kubectl proxy --accept-hosts '.*' "

# idrac

idrac() {
    moob -vm "idrac-$1" -u root -p '18afr8f1!@' -a jnlp
}

# kubectl plugins
export PATH=$PATH:/Users/gochoa/code/src/code.squarespace.net/sre/kubectl-plugins

# Helper Functions

function kctx() {
  if [ "$#" = 0 ]; then
    kubectl config get-contexts
  else
    kubectl config use-context "$@"
  fi
}

function kctx_short() {
  kubectl config get-contexts | rg "*" | awk '{print $2}'
}

function kctx_envs() {
  kctx | sed 1d | sed 's/*/ /' | awk '{print $1}' | tr '\n' ' '
}

# Determines if the given kubectl command will output a YAML document
function kubectl_is_output_yaml() {
  echo "$@" | rg -e "-o yaml"
}

# Determines if the given kubectl command will outut a JSON document
function kubectl_is_output_json() {
  echo "$@" | rg -e "-o json"
}

function kubectl_is_get() {
  echo "$@" | rg -e "get "
}

function kubectl_is_apply() {
  echo "$@" | rg -e "apply "
}

function kubectl_is_logs() {
  echo "$@" | rg -e "logs "
}

# Improved kubectl
function k() {
  # Colorizing for kubectl get
  if [ -n "$(kubectl_is_get \"$@\")" ]; then
    if [ -n "$(kubectl_is_output_yaml \"$@\")" ] && [ $(which bat) ]; then
      kubectl "$@" | bat -l yaml --theme=TwoDark
    elif [ -n "$(kubectl_is_output_json \"$@\")" ] && [ $(which bat) ]; then
      kubectl "$@" | bat -l json --theme=TwoDark
    else
      kubectl "$@"
    fi

  # Log formating through structy
  elif [ -n "$(kubectl_is_logs \"$@\")" ]; then
    kubectl "$@" | structy

  # Safer env/DC-aware kubectl apply
  elif [ -n "$(kubectl_is_apply \"$@\")" ]; then
    cur_ctx=$(kctx_short)
    cur_path=$(pwd)
    all_ctxs=$(kctx_envs)

    # Match pwd against known contexts to determine whether directory
    # is in a cluster context
    pwd_is_mismatch=0
    for ctx in $(echo $all_ctxs); do
      match=$(echo "$cur_path" | rg "$ctx")
      if [ -n "$match" ] && [ "$cur_ctx" != "$ctx" ]; then
        pwd_is_mismatch=1
        echo "Refusing to apply cluster-specific resources to incorrect cluster. Got $ctx, expected $cur_ctx"
      fi
    done

    if [ $pwd_is_mismatch = 0 ]; then
      kubectl "$@"
    fi

  # Normal kubectl
  else
    kubectl "$@"
  fi
}

# List the pods that are running on a given kube node
function kubectl-podhost() {
  node="$1"
  if [ -z "$node" ]; then
    echo "Must provide kubenode"
    exit 1
  fi

  kubectl get pods \
    --template "{{range .items}}{{if eq .spec.nodeName \"$node\"}}{{.metadata.name}}{{\"\n\"}}{{end}}{{end}}" \
    --all-namespaces
}

# Start minikube with docker registry enabled for local dev
function mk() {
  echo "Starting minikube with local registry"
  minikube start --insecure-registry localhost:5000
  eval $(minikube docker-env)
  kubectl --context=minikube apply -f https://gist.github.com/coco98/b750b3debc6d517308596c248daf3bb1
}

function all-kubeclusters() {
  kubectl --context eqx-dal-stage "$@"
  kubectl --context drt-ewr-stage "$@"
  kubectl --context eqx-dal-corp "$@"
  kubectl --context drt-ewr-corp "$@"
  kubectl --context eqx-dal-prod "$@"
  kubectl --context drt-ewr-prod "$@"
}

# Google Cloud

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/gochoa/Downloads/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/gochoa/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/gochoa/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/gochoa/Downloads/google-cloud-sdk/completion.zsh.inc'; fi

# KDD templating + fuzzzzzzzzy
function kdtemplate() {
  local tmpl_dir="deploy"
  if [ ! -z "$1" ]; then
    tmpl_dir="$1"
  fi
  echo "Rendering templates in directory: $tmpl_dir"

  docker run \
        -v "$(pwd)/$tmpl_dir:/code" \
        -e PLUGIN_TEMPLATES="/code" \
        -e ANSIBLE_VAULT_PASSWORD="$(cat ~/.ansible-vault-pw)" \
        quay.squarespace.net/sre/kube-drone-template:3
}

function kdtemplate_fzf() {
  tmpl_dir="$(find . -type d | cut -c 3- | fzf)"
  if [ ! -z "$tmpl_dir" ]; then
    kdtemplate "$tmpl_dir"
  fi
}
alias 'kdtemplate?'='kdtemplate_fzf'

# fzf
function _fzf_custom() {
  target=""
  if [ -z "$2" ]; then
    target=$(fzf)
  else
    target=$(find "$2" | fzf)
  fi

    if [ ! -z "$target" ]; then
    "$1" "$target"
  fi
}

# fzf directories
function _fzf_directory_custom() {
  target=""
  if [ -z "$2" ]; then
    target=$(fzf)
  else
    target=$(find "$2" -type d | fzf)
  fi

  if [ ! -z "$target" ]; then
    "$1" "$target"
  fi
}

function vi_fzf() {
  _fzf_custom vi "$1"
}
function code_fzf() {
  _fzf_custom code "$1"
}
function cd_fzf() {
  _fzf_custom cd "$(pwd)$1"
}
alias 'vi?'=vi_fzf
alias 'code?'=code_fzf
alias 'cd?'=cd_fzf

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Description : find_numina_sensors find the numina sensors on your local /24 subnet
#
# Example: find_numina_sensors 
function find_numina_sensors() {
  wifi_interface="en0"
  cidr=$(ifconfig $wifi_interface | grep -w inet | awk '{print $2}' | cut -d . -f1-3 )
  cidr+='.0/24'
  echo "CIDR: $cidr"
  sudo nmap -sn $cidr | grep -B2 Nvidia | awk '/Nmap scan report for/{printf $5;}/MAC Address:/{print " => "$3;}'
}